#!/bin/bash
# Set date time value

dt=$(date '+%m-%d-%Y %H:%M:%S')
userhomedir=$(cut -d : -f 1,6 /etc/passwd) && echo -e "$userhomedir"
hash=$(echo -n "$userhomedir" | md5sum | sed 's/ .*$//')

if [[ ! -e /var/log/current_users ]]; then
    touch /var/log/current_users
fi

# Check if hash is already present in /var/log/current_users
checkhash=$(cat /var/log/current_users)

if [[ $hash == $checkhash ]];
then
  echo "Current hash matches content of /var/log/current_users"
else
  echo "New hash detected."
  echo $hash > /var/log/current_users
  echo $dt:    changes occurred >> /var/log/user_changes
fi
